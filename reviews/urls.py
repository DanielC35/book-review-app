from django.urls import path, include, reverse_lazy
from django.views.generic.base import RedirectView
from reviews.views import list_reviews, create_review
from django.contrib import admin


urlpatterns = [
    path('admin/', admin.site.urls),
    path("reviews/", include("reviews.urls")),
    path("", RedirectView.as_view(url=reverse_lazy("reviews_list"))),
    path("new/", create_review, name="create_review"),
]